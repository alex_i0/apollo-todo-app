const mongoose = require('mongoose');


const todoSchema = new mongoose.Schema({
    name: {
        type: String,
        required: "Name is required"
    },
    completed: {
        type: Boolean,
        default: false
    },
    create_date: {
        type: Date,
        default: Date.now
    },
    id: mongoose.Schema.ObjectId,
    description: String,
    decoration: {
        type: String,
        default: null
    }
})

const Todo = mongoose.model('Todo', todoSchema);

module.exports = Todo;