import React from 'react';
import classes from './TodoExpand.css';

const todoExpand = (props) => {

    const displayStyle = {
        display: props.visibility ? "block" : "none"
    };

    return(
        <div className={classes.container} style={displayStyle}>
            <p>{props.data.description}</p>
            <p>{props.data.create_date.slice(0,10)}</p>
        </div>
    );
}


export default todoExpand;