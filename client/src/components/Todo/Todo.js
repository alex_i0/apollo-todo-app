import React, { Component } from 'react';
import classes from './Todo.css';
import TodoExpand from '../TodoExpand/TodoExpand';
import axios from 'axios';

class Todo extends Component {

    state = {
        extension: false
    }

    todoExpandDisplayHandler = () => {
        this.setState(prevState => {
            return { extension: !prevState.extension };
        });
    }

    removeTodoHandler = (id) => {
        console.log(id);
        axios.delete('/api/todos/' + id)
            .then(response => {
                console.log(response);
            });
        this.props.serverFetch();
    }

    completeTodoHandler = (id) => {
        let todoObject = { ...this.props.todo };
        const updatedTodo = {
            ...todoObject,
            completed: !this.props.todo.completed
        }

        axios.put('/api/todos/' + id, updatedTodo)
            .then(response => {
                console.log(response);
            });
        this.props.serverFetch();
    }


    render() {

        const colorStyle = {
            backgroundColor: this.props.todo.completed ? "#10ac84" : "#ee5253"
        };

        const icon = this.props.todo.completed === true ? "far fa-times-circle fa-2x": "far fa-check-circle fa-2x" ;
        return (

            <div className={classes.container} >
                <div className={classes.panel} style={colorStyle}>
                    <div className={classes.eject} onClick={this.todoExpandDisplayHandler}>
                        <i className="far fa-arrow-alt-circle-down fa-2x"></i>
                    </div>

                    <h1>{this.props.todo.name}</h1>

                    <div className={classes.done} onClick={() => this.completeTodoHandler(this.props.todo._id)}>
                        <i className={icon}></i>
                    </div>

                    <div className={classes.delete} onClick={() => this.removeTodoHandler(this.props.todo._id)}>
                        <i className="fas fa-trash-alt fa-2x"></i>
                    </div>
                </div>

                <TodoExpand
                    visibility={this.state.extension}
                    data={this.props.todo} />

            </div>

        );
    }

}

export default Todo;