import React from 'react';
import Todo from '../Todo/Todo';
import NewTodo from '../NewTodo/NewTodo';
import classes from './TodoList.css';
import Spinner from '../UI/Spinner/Spinner';

const TodoList = (props) => {

    let todoBlock = <Spinner />;

    if (props.todos != null) {
        todoBlock = (
            <div>
                {props.children}
                {props.todos.map((todo) => {
                    return (
                        <Todo
                            key={todo._id}
                            todo={todo}
                            serverFetch={props.serverFetch}
                        />
                    );

                })}

                <NewTodo
                    serverFetch={props.serverFetch}
                />
            </div>
        );
    }

    return (
        <div className={classes.container}>
            {todoBlock}
        </div>
    );
}

export default TodoList;