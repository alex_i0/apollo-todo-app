import React, { Component } from 'react';
import classes from './NewTodo.css';
import axios from '../../../node_modules/axios';
import Aux from '../../hoc/Auxiliary/Auxiliary'

class NewTodo extends Component {

	state = {
		name: '',
		description: '',
		displayComplex: false,
		isComplete: {
			text: "Incomplete",
			color: "#ee5253",
			condition: false
		}
	}

	postDataHandler = () => {
		const newTodo = {
			name: this.state.name,
			description: this.state.description,
			completed: this.state.isComplete.condition
		}
		console.log(newTodo)
		axios.post('/api/todos', newTodo);
		this.props.serverFetch();
		console.log("Server post");
		this.setState({ displayComplex: false, name: '', description: '' });
	}

	formComplexityHandler = () => {
		this.setState(prevState => ({ displayComplex: !prevState.displayComplex }));
	}

	isCompleteButtonHandler = () => {
		if (this.state.isComplete.text === "Incomplete") {
			let positiveButton = {
				text: "Complete",
				color: "#10ac84",
				condition: true
			}
			this.setState(() => ({ isComplete: positiveButton }));

		} else if (this.state.isComplete.text === "Complete") {
			let negativeButton = {
				text: "Incomplete",
				color: "#ee5253",
				condition: false
			}
			this.setState(() => ({ isComplete: negativeButton }));
		}
	}

	render() {

		const displaySimple = {
			display: this.state.displayComplex === false ? "block" : "none"
		};
		const displayComplex = {
			display: this.state.displayComplex === true ? "block" : "none"
		}
		const buttonSet = {
			backgroundColor: this.state.isComplete.color
		}


		return (
			<Aux>

				<div className={classes.complex} style={displayComplex}>
					<div className={classes.complexContent}>
						<span onClick={this.formComplexityHandler}>X</span>
						<h2>New Todo</h2>
						<hr />

						<input value={this.state.name} onChange={(event) => (this.setState({ name: event.target.value }))} placeholder="Title" type="text" />
						<textarea value={this.state.description} onChange={(event) => this.setState({ description: event.target.value })} placeholder="Description" type="text" />
						<button style={buttonSet} onClick={this.isCompleteButtonHandler}>{this.state.isComplete.text}</button>
						<button onClick={this.postDataHandler} >Add Todo</button>

					</div>
				</div>


				<div className={classes.simple} style={displaySimple}>
					<input value={this.state.name} onChange={(event) => this.setState({ name: event.target.value })} placeholder="Title" type="text" />
					<button onClick={this.postDataHandler} >Add Todo</button>
					<button onClick={this.formComplexityHandler} >More</button>

				</div>

			</Aux>
		);
	}

}

export default NewTodo;