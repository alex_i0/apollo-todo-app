import React from 'react';
import classes from './Header.css';

const header = (props) => {
    return(
        <div className={classes.container}>
            <div className={classes.logo}></div>
            <i className="fas fa-bars fa-2x" onClick={props.clicked}></i>
        </div>
    );
}

export default header;