import React from 'react';
import classes from './Sidebar.css';

const sidebar = (props) => {

    const display = {
        display: props.toggle ? "block" : "none"
    };

    return(
        <div className={classes.container} style={display}>
            <i className="fas fa-times fa-2x" onClick={props.clicked}></i>
                <ul>
                    <li>Home</li>
                    <li>Options</li>
                    <li>Credits</li>
                </ul>
        </div>
    );
}

export default sidebar;