import React, { Component } from 'react';
import classes from './App.css';
import TodoPanel from './containers/todoPanel';
import Header from './components/UI/Header/Header';
import Sidebar from './components/UI/Sidebar/Sidebar';

class App extends Component {

  state={
      sidebarToggle: false
  }

  toggleSidebarHandler = () => {
    this.setState( prevState => {
      return {sidebarToggle: !prevState.sidebarToggle};
    });
  }

  closeSidebarHandler = () => {
    this.setState({sidebarToggle: false});
  }

  render() {
    return (
      <div className={classes.App}>
        <Sidebar toggle={this.state.sidebarToggle} clicked={this.closeSidebarHandler}/>
        <Header clicked={this.toggleSidebarHandler} />
        <TodoPanel />
      </div>
    );
  }
}

export default App;
