import React, { Component } from 'react';
import TodoList from '../components/TodoList/TodoList';
import axios from 'axios';
import classes from './todoPanel.css';

class TodoPanel extends Component {

    state = {
        todos: null,
        search: '',
        filterButton: {
            text: "All",
            bgColor: "#ddd",
            color: "#666",
            condition: null
        }
    }

    todoServerFetch = () => {
        axios.get("/api/todos")
            .then(todos => {
                console.log(todos);
                this.setState({ todos: todos.data })
            })
            .catch(error => {
                alert("Something goes wrong :( Please try refresh site");
            });
    }

    componentDidMount() {
        this.todoServerFetch();
    };

    filterButton = () => {
        if (this.state.filterButton.condition === null) {
            let positiveButton = {
                text: "Complete",
                bgColor: "#10ac84",
                color: "#fff",
                condition: true
            }
            this.setState(() => ({ filterButton: positiveButton }));
        }
        else if (this.state.filterButton.condition === true) {
            let negativeButton = {
                text: "Incomplete",
                bgColor: "#ee5253",
                color: "#fff",
                condition: false
            }
            this.setState(() => ({ filterButton: negativeButton }));

        } else if (this.state.filterButton.condition === false) {
            let neutralButton = {
                text: "All",
                bgColor: "#ddd",
                color: "#666",
                condition: null
            }
            this.setState(() => ({ filterButton: neutralButton }));
        }
    }


    render() {

        let todos = this.state.todos;
        if (this.state.filterButton.condition === true) {
            todos = todos.filter(todo => { return todo.completed === true })
        } else if (this.state.filterButton.condition === false) {
            todos = todos.filter(todo => { return todo.completed === false })
        }

        let searchedTodos = todos;
        if (this.state.search !== '') {
            searchedTodos = todos.filter(todo => { return todo.name.toLowerCase().includes(this.state.search.toLowerCase()) })
        }

        console.log(todos);

        const buttonSet = {
            backgroundColor: this.state.filterButton.bgColor,
            color: this.state.filterButton.color
        }

        return (
            <React.Fragment>
                <TodoList
                    todos={searchedTodos}
                    removed={() => this.removeTodoHandler()}
                    completed={() => this.completeTodoHandler()}
                    serverFetch={() => this.todoServerFetch()}
                >
                    <div className={classes.panel}>
                        <h3>Yours todo list</h3>
                        <div className={classes.panelFilter}>
                            <input type="search" placeholder="Search" value={this.state.search} onChange={(event) => this.setState({ search: event.target.value })} />
                            <button style={buttonSet} onClick={this.filterButton}>{this.state.filterButton.text}</button>
                        </div>
                    </div>
                </TodoList>
            </React.Fragment>
        );
    }
}

export default TodoPanel;