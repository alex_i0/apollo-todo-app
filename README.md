# Apollo Todo App

Apollo is a simple todo app prepared in react and node.

## Getting Started

1. Clone project repository.
2. Install dependencies by `npm start` or `yarn`.
3. 

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo


## Authors

* **Alek Karp**

## License

This project is licensed under the MIT License

